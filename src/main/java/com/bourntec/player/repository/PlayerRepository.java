package com.bourntec.player.repository;
import java.util.Collection;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bourntec.player.model.Player;
import com.bourntec.player.model.PlayerDetails;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
@Repository
public interface PlayerRepository extends JpaRepository<Player,Integer>,JpaSpecificationExecutor<Player>{
  List<Player>findByName(String name);
  List<Player>findByNameAndAge(String name,int age);
  List<Player>findByNameOrAge(String name,int age);
  List<Player>findByNameIgnoreCase(String name);
  List<Player>findByAgeGreaterThan(int age);
  List<Player>findByNameStartingWith(char a);
  List<Player>findByNameLike(String name);
  List<Player>findByAgeOrderByNameDesc(int age);
  List<Player>findByAgeBetween(int start,int end);
  
  @Modifying
  @Transactional
  @Query(value = "update players set age=age+:age", nativeQuery = true)
  void incrementAge(int age);
  
  @Modifying
  @Transactional
  @Query(value = "delete from players where age>:age", nativeQuery = true)
  void deleteMultiple(int age);
  
  @Query(value="select name,team,age from players",nativeQuery=true)
  List<PlayerDetails> findAllPlayers();
  
//  @Query(value = "update players set is_active=false where id=:id", nativeQuery = true)
//  void deleteActive(int id);
  
  List<Player>findByRecordStatus(String recordStatus);
//Collection<PlayerResponseDto> saveAll();
  PlayerResponseDto save(PlayerRequestDto playerRequestDTO);
  
  //List<Player> saveAll(List<PlayerRequestDto> player);
  
  //List<PlayerResponseDto> saveAll(List<PlayerRequestDto> playerr);
  Player findByIdAndRecordStatus(int id,String recordStatus);
//List<Player> findByIsActiveTrueAndId(int id);
  
//  @Modifying
//  @Transactional
//  @Query(value = "update players set (name=:name,age=:age) where id=:id", nativeQuery = true)
//  Player update(int id);
  
  
}

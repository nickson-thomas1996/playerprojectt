package com.bourntec.player.requestdto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import com.bourntec.player.model.Gender;
import com.bourntec.player.model.Player;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencsv.bean.CsvDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerRequestDto {
    
	Integer id;
	@NotBlank(message="Name cannot be empty")
	@Size(min=4,max=30)
	String name;
	String team;
	@NotNull(message ="Age must not be below 14")
	@Min(14)
	Integer age;
	@CsvDate(value = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd")
	LocalDate dateofbirth;
	Gender gender;
	@Email
	String email;

	
	
	
	public Player ConvertToModel()
	{
		Player player=new Player();
		
		BeanUtils.copyProperties(this, player);
//		player.setId(id);
//		player.setName(name);
//		player.setTeam(team);
//		player.setAge(age);
//		player.setDateofbirth(dateofbirth);
//		player.setGender(gender);
		return player;
	}
	
}

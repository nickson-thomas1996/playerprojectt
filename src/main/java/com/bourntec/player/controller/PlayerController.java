package com.bourntec.player.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.model.PlayerDetails;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.search.SearchRequest;
import com.bourntec.player.service.PlayerService;

@RestController
@RequestMapping("/players")
public class PlayerController {
    @Autowired
    PlayerService playerservice;
	@GetMapping("/")
	public String getMessage()
	{
		return "hello";
	}
	@GetMapping
	public List<PlayerResponseDto> getAll()
	{
		return playerservice.getAll();
	}
	@GetMapping("/get")
	public List<Player> findByName(String name)
	{
		return playerservice.findByName(name);
		
	}
	@GetMapping("/nameandage")
	public List<Player> findByNameAndAge(String name,int age)
	{
		return playerservice.findByNameAndAge(name,age);
		
	}
	@GetMapping("/nameorage")
	public List<Player> findByNameOrAge(String name,int age)
	{
		return playerservice.findByNameOrAge(name,age);
		
	}
	@GetMapping("/getcase")
	public List<Player> findByNameIgnoreCase(String name) {

	return playerservice.findByNameIgnoreCase(name);
	}
	
	@GetMapping("/greater")
	public List<Player> findByAgeGreaterThan(int age)
	{
		return playerservice.findByAgeGreaterThan(age);
		
	}
	@GetMapping("/getstart")
	public List<Player> findByNameStartingWith(char a){

	return playerservice.findByNameStartingWith(a);
	}
	@GetMapping("/getlike")
	public List<Player> findByNameLike(String name){

	return playerservice.findByNameLike(name);
	}
	@GetMapping("/getorderby")
	public List<Player> findByAgeOrderByNameDesc(int age){

	return playerservice.findByAgeOrderByNameDesc(age);
	}
	@GetMapping("/getbetween")
	public List<Player>findByAgeBetween(@RequestParam int start, @RequestParam int end) 
	{
	
	return playerservice.findByAgeBetween(start,end);
	}
	
	@DeleteMapping("/{id}")
	public String delete(@PathVariable int id)
	{
		return playerservice.deleteById(id);
	}
	@PostMapping
	public ResponseEntity create(@RequestBody @Valid PlayerRequestDto playerRequestDTO)
	{
		return ResponseEntity.status(HttpStatus.CREATED).body(playerservice.save(playerRequestDTO));
	}
	
//	@PostMapping("/update/{id}")
//	public Player update(@RequestBody Player player,@PathVariable int id)
//	{
//		return playerservice.update(id,player);
//	}
	
	@PostMapping("/bulk")
	public List<PlayerResponseDto> createAll(@RequestBody List<PlayerRequestDto> playerRequestDTO )
	{
		return playerservice.saveAll(playerRequestDTO);
	}
	@PutMapping("/increment-age")
	public void incrementAge(int age) throws FileErrorEXception 
	{
		playerservice.incrementAge(age);
	}
	@DeleteMapping("/delete-age")
	public void deleteMultiple(int age) 
	{
		playerservice.deleteMultiple(age);
	}
	

//	@GetMapping("/{id}")
//	public Player get(@PathVariable int id) throws FileErrorEXception
//	{
//	return playerservice.findById(id);
//	}
	
	@GetMapping("/{id}")
	public PlayerResponseDto get(@PathVariable int id) throws FileErrorEXception
	{
	return playerservice.findById(id);
	}
	
	@ExceptionHandler
	public ResponseEntity<String> handleException(FileErrorEXception e)
	{
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getErrorMessage());
	}
	
	@PostMapping("/{id}")
	public PlayerResponseDto update(@PathVariable int id, @RequestBody PlayerRequestDto playerRequestDTO) throws FileErrorEXception {
		playerRequestDTO.setId(id);
	return playerservice.update(id,playerRequestDTO );
	}
	
	@GetMapping("/search/dynamic")
	public List<Player> search(SearchRequest searchRequest) {
	return playerservice.search(searchRequest);
	}
	
//	@GetMapping("/getcsv")
//	public List<Player> importPlayerFromCsv() {
//	return playerservice.importPlayerFromCsv();
//	}
	
	@GetMapping("/playerss")
	public List<PlayerDetails> findAllPlayers() {
	return playerservice.findAllPlayers();
	}
	
	@GetMapping("/download/csv")
	public void downloadAsCsv() {
	playerservice.downloadAsCsv();
	}
	
	@GetMapping("/download/json")
	public void downloadAsJson() {
	playerservice.downloadAsJson();
	}
	
	@DeleteMapping("active/{id}")
	public void deleteActive(@PathVariable int id) throws FileErrorEXception
	{
	playerservice.deleteActive(id);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	public String handleValidationExceptions(MethodArgumentNotValidException ex)
//	{
//		return ex.getMessage();
//	}
	public Map<String, String> handleValidationExceptions(
	MethodArgumentNotValidException ex) {
	Map<String, String> errors = new HashMap<>();
	ex.getBindingResult().getAllErrors().forEach((error) -> {
	String fieldName = ((FieldError) error).getField();
	String errorMessage = error.getDefaultMessage();
	errors.put(fieldName, errorMessage);
	});
	return errors;
	}


	
}

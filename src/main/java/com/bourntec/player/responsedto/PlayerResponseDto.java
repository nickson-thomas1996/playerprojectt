package com.bourntec.player.responsedto;

import java.time.LocalDate;
import org.springframework.beans.BeanUtils;
import com.bourntec.player.model.Gender;
import com.bourntec.player.model.Player;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerResponseDto {
	
	Integer id;
	String name;
	String team;
	Integer age;
	LocalDate dateofbirth;
	Gender gender;
	
	public PlayerResponseDto(Player player)
	{
		
//        id=player.getId();
//		name=player.getName();
//		team=player.getTeam();
//		age=player.getAge();
//		dateofbirth=player.getDateofbirth();
//		gender=player.getGender();
		
		BeanUtils.copyProperties(player, this);
	
	}

}


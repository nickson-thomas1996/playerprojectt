package com.bourntec.player.util;

public class Constants {
  public static final String SUCCESS_FOLDER="Success";
  public static final String FAILURE_FOLDER="Failure";
  public static final String FOLDER_SEPERATOR="\\";
  public static final String CSV_FILE_EXTENSION=".csv";
  public static final String JSON_FILE_EXTENSION=".json";
  public static final String RECORD_TRUE="A";
  public static final String RECORD_FALSE="D";
}

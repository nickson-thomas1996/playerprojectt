package com.bourntec.player.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencsv.bean.CsvDate;

import lombok.Data;
@Entity
@Table(name="players")
@EntityListeners(AuditingEntityListener.class)
//@Getter
//@Setter
//@AllArgsConstructor
@Data
public class Player {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Integer id;
	
	 private String name,team;
	 @CsvDate(value = "yyyy-MM-dd")
	 @JsonFormat(pattern = "yyyy-MM-dd")
	 @Column(name="dob")
	 private LocalDate dateofbirth;
	 private Integer age;
	 @Enumerated(EnumType.STRING)
	 private Gender gender;
	 @Column (length=1)
	 private String recordStatus;
//	 private Boolean isActive;
	 
	@JsonFormat(pattern = "yyyy-MM-dd")
	@CreatedDate
	LocalDateTime dateCreated;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@LastModifiedDate
	LocalDateTime lastDateModified;
	
}

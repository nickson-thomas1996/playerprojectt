package com.bourntec.player.model;

public interface PlayerDetails {

	String getName();
	String getTeam();
	Integer getAge();
	
	default double getAgeee() {
		if(getAge()!=0)
		return getAge()*0.5;
		else
		return 0;
	}
}

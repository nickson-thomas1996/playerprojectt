package com.bourntec.player.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.scheduler.Scheduler;
import com.bourntec.player.service.CsvOperationService;
import com.bourntec.player.service.JsonFileOperationService;
import com.bourntec.player.service.PlayerService;
import com.bourntec.player.util.Constants;


@Service
@Component

public class Reader {
	final Logger log=LoggerFactory.getLogger(getClass());
	
	@Autowired
	CsvOperationService csvOperationService;
	@Autowired
	PlayerService playerService;
	
	@Autowired
	JsonFileOperationService jsonFileOperationService;
	
	@Value("${file.path.csv}")
	String filePathCsv;
	@Value("${file.path.json}")
	String filePathJson;
	@Scheduled(cron = "${cron.expression}")  

	public void read()  {
		
		try {
			readFiles(Constants.CSV_FILE_EXTENSION,filePathCsv);
			readFiles(Constants.JSON_FILE_EXTENSION,filePathJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}
	
		public void readFiles(String type,String filePath) throws IOException {
            
		    
		    log.info("Entering Into Csv Read");

			
		    List<String> fileNames = getFileList(type,filePath);
		    for (String fileName : fileNames) {
					String file =filePath+ Constants.FOLDER_SEPERATOR + fileName;
					String target = null;
			try {
                if(Constants.CSV_FILE_EXTENSION.equals(type)) {
				playerService.saveAll(csvOperationService.read(file));}
                else if(Constants.JSON_FILE_EXTENSION.equals(type)) {
                	playerService.saveAll(jsonFileOperationService.read(file));
                } 
                else {
                	return;
                }
				target = filePath +Constants.FOLDER_SEPERATOR +Constants.SUCCESS_FOLDER + Constants.FOLDER_SEPERATOR+fileName;
			    }
				catch(FileErrorEXception e) {
					
					target = filePath +Constants.FOLDER_SEPERATOR +Constants.FAILURE_FOLDER + Constants.FOLDER_SEPERATOR + fileName;
				}
				finally {
					moveFile(file,target);
				}}
				}
				void moveFile(String file,String target)
				{
						
						try {
							Files.move(new File(file).toPath(), new File(target).toPath(), StandardCopyOption.REPLACE_EXISTING);
						} catch (Exception e) {
						
							e.printStackTrace();
						}
				   
				}
	    private  List<String> getFileList(String type,String filePath)
	    {
		File directoryPath=new File(filePath);
		
		String contents[]=directoryPath.list();
		return Arrays.asList(contents).stream().
				filter(fileName->fileName.endsWith(type))
				.collect(Collectors.toList());
	    }
	}
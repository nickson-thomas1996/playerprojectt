package com.bourntec.player.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.bourntec.player.model.Player;

public class GenericSpecification<T> implements Specification<T>{

	private SearchRequest searchRequest;
	public GenericSpecification(SearchRequest searchRequest) 
	    {
        this.searchRequest = searchRequest;
        }
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		
		 switch(searchRequest.getOperation()) {
		
		 case EQUALS:
		 return criteriaBuilder.equal(root.get(searchRequest.getField()), searchRequest.getValue());
		 
		 case GREATERTHAN :
		 return criteriaBuilder.greaterThan(root.get(searchRequest.getField()), searchRequest.getValue());
		
		 case LESSTHAN:
		 return criteriaBuilder.lessThan(root.get(searchRequest.getField()), searchRequest.getValue());
		 
		 case GREATERTHANEQUALSTO:
		 return criteriaBuilder.greaterThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());
		 
		 case LESSTHANEQUALSTO:
		 return criteriaBuilder.lessThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());
		 
		 default:
		 return null;
		
		 }


		
		
		
		
		
		
//		if (searchRequest.getOperation()==Operation.EQUALS)
//			return criteriaBuilder.equal(root.get(searchRequest.getField()), searchRequest.getValue());
//			else if (searchRequest.getOperation()==Operation.GREATERTHAN)
//			return criteriaBuilder.greaterThan(root.get(searchRequest.getField()), searchRequest.getValue());
//			else if (searchRequest.getOperation()==Operation.LESSTHAN)
//			return criteriaBuilder.lessThan(root.get(searchRequest.getField()), searchRequest.getValue());
//			else if (searchRequest.getOperation()==Operation.GREATERTHANEQUALSTO)
//			return criteriaBuilder.greaterThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());
//			else if (searchRequest.getOperation()==Operation.LESSTHANEQUALSTO)
//			return criteriaBuilder.lessThanOrEqualTo(root.get(searchRequest.getField()), searchRequest.getValue());
//			else
//			return null;
			}	}

	


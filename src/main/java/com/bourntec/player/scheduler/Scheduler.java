package com.bourntec.player.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
@EnableScheduling
@SpringBootApplication
@Component
public class Scheduler {
//	@Scheduled(fixedDelay=1000*5,initialDelay=10)
//	public void read()
//	{
//		System.out.println("hello");
//	}

	final Logger log=LoggerFactory.getLogger(Scheduler.class);
	@Scheduled(cron="*/2 * * * * *")
	public void read()
	{
		log.info("HellO");
	}
	
}

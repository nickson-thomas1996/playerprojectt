package com.bourntec.player.service;

import java.io.IOException;
import java.util.List;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;

public interface JsonFileOperationService {
  
	List<PlayerRequestDto> read(String filename) throws FileErrorEXception, IOException;
	
	void write(List<PlayerResponseDto> playerList,String fileName) ;
}

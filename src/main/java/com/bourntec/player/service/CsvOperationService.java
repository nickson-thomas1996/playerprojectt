package com.bourntec.player.service;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;

public interface CsvOperationService {
   
	List<PlayerRequestDto> read(String filename) throws FileErrorEXception, IOException;
	void write(List<PlayerResponseDto> list,String fileName) ;
	
	default String csvConversion(PlayerResponseDto playerResponseDto) {
		
		   return new StringBuffer().append(playerResponseDto.getName()).append(",").append(playerResponseDto.getTeam()).append(",")
				  .append(playerResponseDto.getAge()).append(",").append(playerResponseDto.getDateofbirth()).append(",")
			    		.append(playerResponseDto.getGender()).append("\n").toString();
		        }
	//void WriteToJson(List<Player> playerList,String fileName) ;
}

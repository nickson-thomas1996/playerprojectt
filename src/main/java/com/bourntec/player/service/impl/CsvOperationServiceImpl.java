package com.bourntec.player.service.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


import com.bourntec.player.model.Gender;
import com.bourntec.player.model.Player;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.service.CsvOperationService;

//@Primary
@Service
public class CsvOperationServiceImpl implements CsvOperationService {
    
	
	@Override
	public List<PlayerRequestDto> read(String filename) {
		List<PlayerRequestDto> playerList = new ArrayList<>();
		try {
		FileReader fileReader=new FileReader(filename);
		Scanner scanner=new Scanner(fileReader);
		while(scanner.hasNextLine())
		{   
			String[] playerDetail=scanner.nextLine().split(",");
			PlayerRequestDto player=new PlayerRequestDto();
			player.setName(playerDetail[0]);
			player.setTeam(playerDetail[1]);
			player.setAge((Integer.parseInt(playerDetail[2])));
			player.setDateofbirth(LocalDate.parse(playerDetail[3]));
			player.setGender(Gender.valueOf(playerDetail[4]));
			
			playerList.add(player);
           
		}}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		return playerList;
	}

	@Override
	public void write(List<PlayerResponseDto> playerList,String fileName)  {
		
		FileWriter file;
		try {
			file = new FileWriter("D://download.csv");
			for (int i = 0; i < playerList.size(); i++) {
				String data = csvConversion(playerList.get(i));
				file.write(data);}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
//		file.close();

//	@Override
//	public void WriteToJson(List<Player> playerList, String fileName) {
//		// TODO Auto-generated method stub
//		
//	}
	

}

package com.bourntec.player.service.impl;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.model.PlayerDetails;
import com.bourntec.player.repository.PlayerRepository;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.search.GenericSpecification;
import com.bourntec.player.search.SearchRequest;
import com.bourntec.player.service.CsvOperationService;
import com.bourntec.player.service.JsonFileOperationService;
import com.bourntec.player.service.PlayerService;
import com.bourntec.player.util.Constants;

@Service
public class PlayerServiceImpl implements PlayerService {
	
	@Value("${csv.download.path}")
	String fileName;
	@Autowired
	PlayerRepository playerRepository;
	@Autowired
	CsvOperationService csvOperationService;
	@Autowired
	JsonFileOperationService jsonFileOperationService;
	
//	@Autowired
//	CsvOperationServiceImpl csvOperationServiceImpl;
//	@Autowired
//	CsvOperationServiceImpl csvOperationServiceImpl;

	
  public List<PlayerResponseDto> getAll()
  {
//	return playerRepository.findAll().stream().
//			map(PlayerResponseDto::new).collect(Collectors.toList());
	
	return playerRepository.findByRecordStatus(Constants.RECORD_TRUE).stream()
			.map(PlayerResponseDto::new)
			.collect(Collectors.toList());
//	map(player->new PlayerResponseDto(player)).
//	collect(Collectors.toList());
  }
  @Override
  public List<Player> findByName(String name){
	  return playerRepository.findByName(name);
  }
  @Override
  public List<Player> findByNameAndAge(String name,int age){
	  return playerRepository.findByNameAndAge(name,age);
  }
  @Override
  public List<Player> findByNameOrAge(String name,int age){
	  return playerRepository.findByNameOrAge(name,age);
  }
  @Override
  public List<Player> findByNameIgnoreCase(String name){
	  return playerRepository.findByNameIgnoreCase(name);
  }
  @Override
  public List<Player> findByAgeGreaterThan(int age){
	  return playerRepository.findByAgeGreaterThan(age);
  }
  @Override
  public List<Player> findByNameStartingWith(char a){
	  return playerRepository.findByNameStartingWith(a);
  }
  @Override
  public List<Player> findByNameLike(String name){
	  return playerRepository.findByNameLike(name);
  }
  @Override
  public List<Player> findByAgeOrderByNameDesc(int age){
	  return playerRepository.findByAgeOrderByNameDesc(age);
  }
  @Override
  public List<Player>findByAgeBetween(int start,int end){
	  
	  return playerRepository.findByAgeBetween(start,end);
  }
  
  @Override
 	public String deleteById(int id)
 	{      
	        if(playerRepository.existsById(id)==true)
	        {
	        	playerRepository.deleteById(id);
	            return "Record Deleted";
	        }
	       else
	            return "record not exist";
 	        
 	}
  @Override
	public PlayerResponseDto save(PlayerRequestDto playerRequestDTO)
	{
		//return playerRepository.save(playerRequestDTO.ConvertToModel());
		
		Player player = playerRequestDTO.ConvertToModel();
		player.setRecordStatus(Constants.RECORD_TRUE);
		player = playerRepository.save(player);
		return new PlayerResponseDto(player);
	
	  }
  
  
  @Override
 	public List<PlayerResponseDto> saveAll(List<PlayerRequestDto> playerRequestDTOList)
 	{
 		//return playerRepository.saveAll(playerList);
 		
 		//Player player = ((PlayerRequestDto) playerRequestDTO).ConvertToModel();
	    
	    
	//   List<PlayerRequestDto> playerr= playerRequestDTO;
	    // List<Player> playerList=playerRequestDTOList.stream().
	    	//	 map(playerRequestDTO->playerRequestDTO.ConvertToModel()).
	    	//	collect(Collectors.toList());
	    List<Player> playerList=new ArrayList<>();
	     Player player=null;
	     for (PlayerRequestDto playerRequestDTO : playerRequestDTOList) {
	    	player=playerRequestDTO.ConvertToModel();
	    	player.setRecordStatus(Constants.RECORD_TRUE);
	    	playerList.add(player);
	    	//result.setIsActive(true);
	    	
	    }
	    return playerRepository.saveAll(playerList).
	    		stream()
	    		.map(PlayerResponseDto::new).toList();    
	    
	    
	    
//	    Iterator iterator = playerr.iterator();
//	      while(iterator.hasNext()) {
//	    	  ((PlayerRequestDto) iterator).ConvertToModel();
//	      }
	    
 		
 		//Player player = playerr.map(playerRequestDTO.ConvertToModel());
 		
 		//return playerRepository.saveAll().stream().map(player->new PlayerResponseDto(player)).
		//collect(Collectors.toList());
 		
		
		//player = playerRepository.save(player);
		
		
		
		//PlayerResponseDto playerResponseDto = new PlayerResponseDto(player);
		//return playerResponseDto;
 	}
  @Override
  @Transactional
  public void incrementAge(int age) throws FileErrorEXception {
	  
	  playerRepository.incrementAge(age);
	  if(true)
		  throw new FileErrorEXception("haaiiiiiiiii");

  }
  @Override
	public void deleteMultiple(int age)
	{
	        playerRepository.deleteMultiple(age);
	}
  
//    @Override
//    public List<Player> search(Player player)
//    {
//	return playerRepository.findAll(Example.of(player));
//    }
//@Override
//public Player update(int id)
//{
//	return playerRepository.update(id);
//}
    @Override
    public PlayerResponseDto update(int id, PlayerRequestDto playerRequestDTO) throws FileErrorEXception {
    	Optional<PlayerResponseDto> playerOptional=Optional.empty();
		if(playerOptional.isPresent())
		{
			PlayerResponseDto existingPlayer=playerOptional.get();
			//playerRequestDTO.setDateCreated(existingPlayer.getDateCreated());
			playerRequestDTO.setId(id);
		   return playerRepository.save(playerRequestDTO);
		}
		else
			throw new FileErrorEXception();
 
    }

@Override
public PlayerResponseDto findById(int id) throws FileErrorEXception
{
	Optional<Player> playerOptional=playerRepository.findById(id);
			if(playerOptional.isPresent())
				return new PlayerResponseDto(playerOptional.get());
			else
				throw new FileErrorEXception("Error ,Data with id "+id+" Not Found");
}
@Override
public List<Player> search(SearchRequest searchRequest) {
    
	return playerRepository.findAll(new GenericSpecification<Player>(searchRequest));
//	return	playerRepository.findAll((root,query,criteriaBuilder)->{
//		if(operation.equals("equals"))return criteriaBuilder.equal(root.get(field),value );
//		else if(operation.equals("greaterthan"))return criteriaBuilder.greaterThan(root.get(field),value );
//		else return criteriaBuilder.lessThan(root.get(field),value );
//		});
}




@Override
public List<PlayerDetails> findAllPlayers() {
	return playerRepository.findAllPlayers();
}

@Override
public void downloadAsCsv() {
	//csvOperationService.WriteToCsv(getAll(),fileName);
	csvOperationService.write(getAll(),fileName);
}
@Override
public void downloadAsJson() {
	jsonFileOperationService.write(getAll(),fileName);
}
@Override
public void deleteActive(int id) throws FileErrorEXception {
	
//	Player player=playerRepository.findByIsActiveTrueAndId(id).get(0);
//	player.setIsActive(false);
//	playerRepository.save(player);
	
	Player player=playerRepository.findByIdAndRecordStatus(id,Constants.RECORD_TRUE);
	if(player!=null) {
		//Player player=playerList.get(0);
		player.setRecordStatus(Constants.RECORD_TRUE);
		playerRepository.save(player);
	}
	else {
		throw new FileErrorEXception("errrorrrr");
		 }
          }
           }
package com.bourntec.player.service.impl;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.service.JsonFileOperationService;
import com.bourntec.player.util.LocalDateAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

@Primary
@Service
public class JsonFileOperationServiceGsonImpl implements JsonFileOperationService {
    
	@Override
	public List<PlayerRequestDto> read(String filename) throws FileErrorEXception, IOException {
//		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Gson gson = new GsonBuilder()
		        .setPrettyPrinting()
		        .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
		        .create();
		JsonReader reader = new JsonReader(new FileReader(filename));
		return gson.fromJson(reader, new TypeToken<List<PlayerRequestDto>>() {}.getType());  
		
		
		
	
	}

	@Override
	public void write(List<PlayerResponseDto> playerList, String fileName) {
		
        FileWriter writer;
		try {
			writer = new FileWriter(fileName);
			new Gson().toJson(playerList,writer);
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

}

package com.bourntec.player.service.impl;

import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Gender;
import com.bourntec.player.model.Player;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.service.CsvOperationService;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
@Service
public class CsvOperationOpenCsvImpl implements CsvOperationService{
   
//	@Primary
	@Override
	public List<PlayerRequestDto> read(String filename) throws FileErrorEXception {
		List<PlayerRequestDto> playerList = new ArrayList<>();
		try {
			// CSVReader csvReader=new CSVReader(new FileReader(filename));
			CSVReader csvReader = new CSVReaderBuilder(new FileReader(filename)).withSkipLines(1).build();
			List<String[]> recordList = csvReader.readAll();
			for (String[] row : recordList) {
				if (row.length == 1 && row[0].isEmpty()) {
					continue;
				}
				PlayerRequestDto player = new PlayerRequestDto();
				player.setName(row[0].toString());
				player.setTeam(row[1].toString());
				player.setAge((Integer.parseInt(row[2])));
				player.setDateofbirth(LocalDate.parse(row[3]));
				player.setGender(Gender.valueOf(row[4]));

				playerList.add(player);
			}
			csvReader.close();
		} catch (IOException e) {
			throw new FileErrorEXception("Excption occured");
		} catch (NumberFormatException e) {
			throw new FileErrorEXception("Excption occured");
		}
//		File file=new File(filename);
//        File file1=new File("D:\\csv\\Success\\"+file.getName());
//		try {
//			Files.move(file.toPath(),file1.toPath(),StandardCopyOption.REPLACE_EXISTING);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		catch (CsvException e) {

			throw new FileErrorEXception("Excption occured");
		}

		return playerList;
	}

	@Override
	public void write(List<PlayerResponseDto> playerList,String fileName)  {
		Writer writer;
		try {
			writer = Files.newBufferedWriter(Paths.get(fileName));
			 StatefulBeanToCsv<PlayerResponseDto> beanToCsv = new StatefulBeanToCsvBuilder(writer)
	                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
	                    .build();

		       beanToCsv.write(playerList);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CsvDataTypeMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CsvRequiredFieldEmptyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

//	@Override
//	public void WriteToJson(List<Player> playerList, String fileName) {
//		// TODO Auto-generated method stub
//		
//	}
}

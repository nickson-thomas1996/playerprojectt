package com.bourntec.player.service.impl;

import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.bind.DataBindingException;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.service.CsvOperationService;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Primary
@Service
public class CsvOperationServiceBeanImpl implements CsvOperationService {

	@Override
	public List<PlayerRequestDto> read(String fileName) throws FileErrorEXception, IOException {
		List<PlayerRequestDto> playerList=null;
		    FileReader fileReader=new FileReader(fileName);
		try {
			playerList = new CsvToBeanBuilder(fileReader).withType(PlayerRequestDto.class).build().parse();
//			fileReader.close();
		} 
		catch (IllegalStateException  e) {
			throw new FileErrorEXception("Error Occured");
		}
		catch(Exception e)
		{
			throw new FileErrorEXception("Error Occured");
		}
		finally {
			fileReader.close();
		}
		return playerList;
	}

	
	
	
//	@Override
//	public List<Coffee> read(String fileName) throws FileParserException {
//	List<Coffee> coffeeList = null;
//	try {
//
//	FileReader fileReader=new FileReader(fileName);
//	coffeeList = new CsvToBeanBuilder( fileReader).withType(Coffee.class).build().parse();
//
//	fileReader.close();
//	} catch (IllegalStateException |FileNotFoundException e) {
//
//	throw new FileParserException("Exception!!");
//
//	} catch (IOException e) {
//
//	e.printStackTrace();
//	}
//
//	return coffeeList;
//	}
	
	
	@Override
	public void write(List<PlayerResponseDto> playerList,String fileName)  {

	            Writer writer;
				try {
					writer = Files.newBufferedWriter(Paths.get(fileName));
					 StatefulBeanToCsv<PlayerResponseDto> beanToCsv = new StatefulBeanToCsvBuilder(writer)
			                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
			                    .build();
				       beanToCsv.write(playerList);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CsvDataTypeMismatchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CsvRequiredFieldEmptyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				
	}




//	@Override
//	public void WriteToJson(List<Player> playerList, String fileName) {
//		
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.findAndRegisterModules();
//		try {
//		mapper.writeValue(Paths.get(fileName).toFile(), playerList);
//		} catch (StreamWriteException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		} catch (DataBindingException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		} catch (IOException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		}
//
//		
//		
//	}
	
	
	
}

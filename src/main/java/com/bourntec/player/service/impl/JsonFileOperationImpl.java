package com.bourntec.player.service.impl;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.DataBindingException;

import org.springframework.stereotype.Service;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.service.JsonFileOperationService;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JsonFileOperationImpl implements JsonFileOperationService {
    

	@Override
	public List<PlayerRequestDto> read(String filename) throws FileErrorEXception {
		ObjectMapper mapper=new ObjectMapper();
		mapper.findAndRegisterModules();
	   log.info("Entering Read Method" ,filename);
		
		List<PlayerRequestDto> playerList=null;
		try {
			playerList=Arrays.asList(mapper.readValue(Paths.get(filename).toFile(), PlayerRequestDto[].class));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		}
	 return playerList;
	}
    

	
//	@Override
//	public void WriteToCsv(List<Player> playerList, String fileName) {
//		// TODO Auto-generated method stub
//
//	}

		@Override
		public void write(List<PlayerResponseDto> playerList, String fileName) {
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.findAndRegisterModules();
			try {
			mapper.writeValue(Paths.get(fileName).toFile(), playerList);
			} catch (StreamWriteException e) {
				log.error(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (DataBindingException e) {
				log.error(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (IOException e) {
				log.error(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			
			
		}
	}



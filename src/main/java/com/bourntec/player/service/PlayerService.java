package com.bourntec.player.service;

import java.util.List;

import com.bourntec.player.exception.FileErrorEXception;
import com.bourntec.player.model.Player;
import com.bourntec.player.model.PlayerDetails;
import com.bourntec.player.requestdto.PlayerRequestDto;
import com.bourntec.player.responsedto.PlayerResponseDto;
import com.bourntec.player.search.SearchRequest;

public interface PlayerService {
//	 List<Player> findAll();
	 List<PlayerResponseDto> getAll();
	 List<Player> findByName(String name);
	 List<Player> findByNameAndAge(String name,int age);
	 List<Player> findByNameOrAge(String name,int age);
	 List<Player> findByNameIgnoreCase(String name);
	 List<Player> findByAgeGreaterThan(int age);
	 List<Player> findByNameStartingWith(char a);
	 List<Player> findByNameLike(String name);
	 List<Player> findByAgeOrderByNameDesc(int age);
	 List<Player> findByAgeBetween(int start,int end);
	 
	 String deleteById(int id);
	 
	 public PlayerResponseDto save(PlayerRequestDto playerRequestDTO);
	 
	// public List<Player> saveAll(List<Player> playerList);
	 
	 void incrementAge(int age) throws FileErrorEXception;
	  
	 void deleteMultiple(int age);
	 
	 List<Player>search(SearchRequest searchRequest);
	 
	 PlayerResponseDto findById(int id) throws FileErrorEXception;
	 
//	Player update(int id);
	 
	 PlayerResponseDto update(int id,PlayerRequestDto playerRequestDTO) throws FileErrorEXception;
	
//	 List<Player> search(String operation,String field,String value);
	 
	 void downloadAsCsv();
	 
	 void downloadAsJson();
	 
	 List<PlayerDetails> findAllPlayers();
	 
	 void deleteActive(int id) throws FileErrorEXception ;
	 
	 List<PlayerResponseDto> saveAll(List<PlayerRequestDto> playerRequestDTO);
	 
}
